import math, time
from Node import Node
from Problem import Problem

class Puzzle(Problem):
    def __init__(self,initial_state=[1,2,3,4,5,6,7,8,0],final_state=[0,1,2,3,4,5,6,7,8]):
        self.size_puzzle=int(math.sqrt(len(initial_state)))
        super().__init__(initial_state,final_state)
    
    def child_node(self,node):#generacion de los hijos
        childs=[] #Lista de hijos
        son=self.move_up(node)
        if(son!=None):
            childs.append(son)
        son=self.move_down(node)
        if(son!=None):
            childs.append(son)
        son=self.move_right(node)
        if(son!=None):
            childs.append(son)
        son=self.move_left(node)
        if(son!=None):
            childs.append(son)
        
        return childs
        
    #Solo se podra realizar movimiento hacia arriba siempre y cuando el espacion en blando
    #no se encuentre en la ultima fila de la matriz
    def move_up(self,node):#Mover arriba
        state=node.State.copy()
        sub_lista=state[(len(state)-self.size_puzzle):len(state)]#Se obtiene la ultima fila de la matriz
        if(0 in sub_lista):
            return None
        else:
            new_state=[]
            count=0
            for value in state:
                if(value==0):
                    new_state.append(state[count+self.size_puzzle])
                    state[count+self.size_puzzle]=0
                    new_state.extend(state[(count+1):len(state)])
                    new_node=Node(new_state,parent=node,action='move_up')
                    return new_node
                else:
                    new_state.append(value)
                count+=1
           
            


    """Solo se podra realizar movimiento hacia abajo siempre y cuando el espacio en blanco
    no este en la primera fila"""
    def move_down(self,node):#Mover abajo
        state=node.State.copy()
        sub_lista=state[0:self.size_puzzle] #Se optiene la ultima fila de la matriz
        if(0 in sub_lista): #Se valida si se encuentra el cero en esa lista
            return None
        else:
            new_state=[]
            count=0
            for value in state:
                if(value==0): 
                    new_state.append(state[count-self.size_puzzle])
                    state[count-self.size_puzzle]=0
                    new_state.extend(state[(count+1):len(state)])
                    new_node=Node(new_state,parent=node,action='move_down')
                    return new_node
                else:
                    new_state.append(value)
                count+=1

    """Solo se podra realizar movimiento hacia la derecha simepre y cuando 
    el espacion en blanco no se encuetre en el extremo izquierdo de la matriz
    """
    def move_right(self,node):#Mover Derecha
        state=node.State.copy()
        for i in range(0,len(state),self.size_puzzle):
            if(state[i]==0): #En caso de que el espacio vacio se encuentre en la izquierda de la matriz
                return None
        
        new_state=[]
        count=0
        for value in state:
            if(value==0):
                tem=new_state[len(new_state)-1]
                new_state[len(new_state)-1]=0
                new_state.append(tem)
                new_state.extend(state[(count+1):len(state)])
                new_node=Node(new_state,parent=node,action='move_right')
                return new_node
            else:
                new_state.append(value)
            count+=1

    """Solo se podra realizar el movimiento a la izquierda siempre y cuando
    el espacion en blanco no se encuentre en la parte extrema derecha de la matriz"""
    def move_left(self,node):#Mover Izquierda
        state=node.State.copy()
        for i in range((self.size_puzzle-1),len(state),self.size_puzzle):
            if(state[i]==0): #En caso de que el espacio vacio se encuentre en la derecha de la matriz
                return None
        
        new_state=[]
        count=0
        for value in state:
            if(value==0): 
                new_state.append(state[count+1])
                state[count+1]=0
                new_state.extend(state[(count+1):len(state)])
                new_node=Node(new_state,parent=node,action='move_left')
                return new_node
            else:
                new_state.append(value)
            count+=1