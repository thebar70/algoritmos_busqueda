class Stack:
    def __init__(self):
        self.items = []

    def __iter__(self):
        for r in self.items:
            yield r

    def isEmpty(self):
        return self.items == []

    def add(self, item):
        self.items.append(item)

    def unstack(self):
        return self.items.pop()

    def inspeccionar(self):
        return self.items[len(self.items)-1]

    def length(self):
        return len(self.items)