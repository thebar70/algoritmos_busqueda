# -*- coding: utf-8 -*-
"""
Created on Tue Aug 21 10:12:28 2018

@author: eumartinez
"""

from BreadthFirst import BreadthFirst
from DepthFirst import DepthFirst
from WaterJug import WaterJug
from Puzzle import Puzzle
'''
pz=Puzzle((1,0,2,8,4,3,7,6,5),(1,2,3,8,0,4,7,6,5))

wj=WaterJug(4,3,(4,3),(2,0))
'''
pz=Puzzle([1,0,2,8,4,3,7,6,5],[1,2,3,8,4,0,7,6,5])

#wj=WaterJug()

'''
bf=BreadthFirst(wj) #se le indica a la busqueda que va a trabajar con el problema de las jarras
'''
bf=DepthFirst(pz)
#bf=BreadthFirst(pz)
sol=bf.run()
print('Solution: '+str(sol))
