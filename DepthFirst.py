from Stack import Stack
from collections import deque
#Clase busqueda en profundidad
class DepthFirst():
 
    def __init__(self,problem):
        self.problem=problem

    
    def run(self):
        open_list=Stack()
        close_list=deque()
        starting_node=self.problem.Initial_Node
        open_list.add(starting_node)
        solution=[]
        
        while open_list.length() > 0:
            current_node=open_list.unstack()
            print('Current_Node:'+str(current_node.State))
            close_list.append(current_node)
            
            if self.problem.goal_test(current_node):
                while current_node.Parent!=None:
                    solution.append(current_node.Action)
                    current_node=current_node.Parent
                solution.reverse()
                return solution 
                        
            children= self.problem.child_node(current_node) 
    
            for child in children :
                if child not in open_list and child not in close_list:
                    print('Child:'+str(child.State))
                    open_list.add(child)
                    
        return solution